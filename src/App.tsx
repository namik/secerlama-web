import React from 'react';
import Header from './components/Header';


class App extends React.Component {
  public render(): JSX.Element {
    return(
      <Header />
    );
  }
}

export default App;
