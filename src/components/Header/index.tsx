import React from 'react';
import Button from '@material-ui/core/Button';

class Header extends React.Component {
    public render():JSX.Element {
        return(
            <Button variant="contained" color="primary">
                Hello World
            </Button>
        );
    }
}

export default Header;